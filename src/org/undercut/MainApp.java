package org.undercut;

import org.undercut.controller.UnderCutController;
import org.undercut.controller.UnderCutControllerImpl;
import org.undercut.model.UnderCutModel;
import org.undercut.model.UnderCutModelImpl;
import org.undercut.view.UnderCutView;
import org.undercut.view.UnderCutViewImpl;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Main class to launch the application.
 */
public class MainApp extends Application {

    private static UnderCutView view;

    /**
     * Main method of the entire program.
     * 
     * @param args
     *            Content
     */
    public static void main(final String[] args) {
        final UnderCutModel model = new UnderCutModelImpl();
        final UnderCutController controller = new UnderCutControllerImpl(model);
        MainApp.view = new UnderCutViewImpl(controller);
        controller.registerView(MainApp.view);
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) {
        MainApp.view.start(primaryStage);
    }

}
