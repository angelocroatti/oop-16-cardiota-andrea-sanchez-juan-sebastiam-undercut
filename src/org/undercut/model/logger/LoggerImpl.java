package org.undercut.model.logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.undercut.model.instantcollection.InstantCollection;
import org.undercut.model.utils.CheckIllegalArgument;
import org.undercut.view.utils.TabName;

/**
 * Class that implements the logic for a logging unit.
 */
public final class LoggerImpl implements Logger {

    private static final String PATH = System.getProperty("user.home")
                                       + System.getProperty("file.separator")
                                       + "UnderCut-Logs"
                                       + System.getProperty("file.separator");
    private static final String EXTENTION = ".txt";
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private static final String DOUBLE_LINE_SEPARATOR = LoggerImpl.LINE_SEPARATOR + LoggerImpl.LINE_SEPARATOR;
    private static final String COMMON_LOGFILE_NAME_PART = "UnderCut-Log";
    private static final String ENCODING_TYPE = "UTF-8";

    private final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.ITALIAN);
    private String pathToFile;
    private int numOfLogs;
    private List<TabName> tabsToLog;

    /**
     * Class constructor of LoggerImpl the class.
     */
    public LoggerImpl() {
        final File dir = new File(LoggerImpl.PATH);
        this.removeAndCreateIfExist(dir);
        this.numOfLogs = 0;
    }

    @Override
    public void setWhatToLog(final List<TabName> whatToLog) throws IOException {
        this.tabsToLog = whatToLog;
        this.numOfLogs++;
        this.prepareFile();
    }

    @Override
    public void doLog(final InstantCollection dataToLog) throws IllegalArgumentException, IOException {
        this.checkInstantCollection(dataToLog);
        for (final TabName tabName : this.tabsToLog) {
            this.writeDataOnFile(tabName, dataToLog.getValuesOf(tabName).toString());
        }
        this.skipLineOnFile();
    }

    /**
     * Method to prepare the log file to write a header line on it.
     * 
     * @throws IllegalArgumentException
     *          If there isn't data to log
     * 
     * @throws IOException 
     *          If there is a problem with the log file
     */
    private void prepareFile() throws IllegalArgumentException, IOException {
        CheckIllegalArgument.checkForIllegalArgument(this.tabsToLog);
        this.pathToFile = PATH + LoggerImpl.COMMON_LOGFILE_NAME_PART + this.numOfLogs + EXTENTION;
        this.writeFileIndex();
    }

    /**
     * Method to write a line of data in the log file.
     */
    private void writeDataOnFile(final TabName tabName, final String data) throws IOException {
        try (BufferedWriter w = new BufferedWriter(
                new OutputStreamWriter(
                  new FileOutputStream(this.pathToFile, true), LoggerImpl.ENCODING_TYPE))) {
                w.write(tabName
                        + (tabName.equals(TabName.INERTIAL) ? "\t" : "\t\t")
                        + data
                        + LoggerImpl.LINE_SEPARATOR);
        }
    }

    /**
     * Method to write a new empty line in the log file.
     */
    private void skipLineOnFile() throws IOException {
        try (BufferedWriter w = new BufferedWriter(
                new OutputStreamWriter(
                  new FileOutputStream(this.pathToFile, true), LoggerImpl.ENCODING_TYPE))) {
                w.write(LoggerImpl.LINE_SEPARATOR);
        }
    }

    /**
     * Method to write the log file's header.
     */
    private void writeFileIndex() throws IOException {
        try (BufferedWriter w = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(this.pathToFile), LoggerImpl.ENCODING_TYPE))) {
            w.write("UnderCut Log File [" + sdf.format(Calendar.getInstance().getTime())
                    + "]" + LoggerImpl.DOUBLE_LINE_SEPARATOR);
            w.write("Log data source:\t");
            for (final TabName tabName : this.tabsToLog) {
                w.write(tabName.toString() + "  ");
            }
            w.write(LoggerImpl.DOUBLE_LINE_SEPARATOR);
        }
    }

    /**
     * Method to clear log's directory at the start of the application.
     * 
     * @throws IllegalStateException
     *          If the system wasn't able to clear the log's directory
     */
    private void removeAndCreateIfExist(final File file) throws IllegalStateException {
        final File[] arrays = file.listFiles();
        if (file.exists() && arrays != null) {
            final List<File> files = Arrays.asList(arrays);
            for (final File tmpFile : files) {
                this.checkIOOperation(tmpFile.delete());
            }
            this.checkIOOperation(file.delete());
        }
        this.checkIOOperation(file.mkdir());
    }

    /**
     * Method to check if the InstantCollection to log is correct (not null or empty).
     * 
     * @throws IllegalArgumentException
     *          If argument is null or contains empty sub-collections
     */
    private void checkInstantCollection(final InstantCollection dataToLog) throws IllegalArgumentException {
        CheckIllegalArgument.checkForIllegalArgument(dataToLog);
        for (final TabName tabName : TabName.values()) {
            if (!tabName.equals(TabName.LOG) && dataToLog.getValuesOf(tabName).isEmpty()) {
                throw new IllegalArgumentException();
            }
        }
    }

    /**
     * Method to check the correct result of IO operation.
     */
    private void checkIOOperation(final boolean operationResult) throws IllegalStateException {
        if (!operationResult) {
            throw new IllegalStateException();
        }
    }
}
