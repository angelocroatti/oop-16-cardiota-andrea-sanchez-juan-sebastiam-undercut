package org.undercut.model.observers;

/**
 * Functional interface to model the concept of observer.
 */
public interface GenericExceptionObserver {

    /**
     * Method to update the observer.
     * 
     * @param obs
     *          The observer manager which is calling
     *
     * @param ex
     *          The exception
     *
     * @param exceptionMessage
     *          The exception message
     */
    void update(ModelExceptionsObserverManager obs, Exception ex, String exceptionMessage);

}
