package org.undercut.model.datafetcher;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.undercut.MainApp;
import org.undercut.model.FieldName;
import org.undercut.model.utils.Pair;

/**
 * Class to retrieve the data needed for the simulation.
 */
public class DataFetcherImpl implements DataFetcher {

    private static final String EXTENSION = ".txt";
    private static final String PATH_TO_TRACK = "/simulationsData/";
    private static final String ENCODING = "UTF-8";

    private Map<FieldName, Pair<InputStream, Scanner>> inputStreams;

    /**
     * Class constructor of the DataFetcherImpl class.
     */
    public DataFetcherImpl() {
        this.initInputStream();
    }

    @Override
    public synchronized Map<FieldName, ? extends Number> getValues() throws IllegalStateException,
                                                                            NumberFormatException,
                                                                            IOException {
        return this.read();
    }

    @Override
    public void reset() throws IOException {
        for (final Pair<InputStream, Scanner> p : this.inputStreams.values()) {
            p.getFirst().close();
        }
        this.initInputStream();
    }

    /**
     * Method to initialise all the input streams from data files.
     */
    private void initInputStream() {
        this.inputStreams = new HashMap<>();
        for (final FieldName field : FieldName.values()) {
            final InputStream in = MainApp.class.getResourceAsStream(DataFetcherImpl.PATH_TO_TRACK 
                                                                   + field.getName()
                                                                   + DataFetcherImpl.EXTENSION);
            this.inputStreams.put(field, new Pair<>(in, new Scanner(in, DataFetcherImpl.ENCODING)));
        }
    }

    /**
     * Method to read one line of each file of the field set.
     * 
     * @return
     *          A raw collection of this data.
     *
     * @throws IllegalStateException
     *          Exception due to a problem during the file reading
     *
     * @throws NumberFormatException
     *          Exception due to a problem during the number casting
     *
     * @throws IOException
     *          Exception due to an I/O problem
     */
    private Map<FieldName, Number> read() throws IllegalStateException, 
                                                 NumberFormatException,
                                                 IOException {
        final Map<FieldName, Number> container = new HashMap<>();
        for (final Map.Entry<FieldName, Pair<InputStream, Scanner>> e : this.inputStreams.entrySet()) {
            container.put(e.getKey(), Double.parseDouble(e.getValue().getSecond().nextLine()));
            if (!e.getValue().getSecond().hasNext()) {
                this.reset();
            }
        }
        return Collections.unmodifiableMap(container);
    }

}
