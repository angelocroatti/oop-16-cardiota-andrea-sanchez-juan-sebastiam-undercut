package org.undercut.model.processingengine;

import java.util.Map;

import org.undercut.model.FieldName;
import org.undercut.model.instantcollection.InstantCollection;
import org.undercut.model.instantcollection.InstantCollectionImpl;
import org.undercut.model.utils.CheckIllegalArgument;

/**
 * Class that implements a logic to process raw data into an organised collection. 
 */
public class ProcessingEngineImpl implements ProcessingEngine {

    @Override
    public InstantCollection processData(final Map<FieldName, ? extends Number> values) throws IllegalArgumentException {
        CheckIllegalArgument.checkForIllegalArgument(values);
        if (values.isEmpty()) {
            throw new IllegalArgumentException();
        }
        final InstantCollectionImpl instantValues = new InstantCollectionImpl();
        values.entrySet().forEach(e -> instantValues.setValueOf(e.getKey(), e.getValue()));
        return instantValues;
    }

}
