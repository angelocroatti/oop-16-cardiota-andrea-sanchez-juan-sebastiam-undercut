package org.undercut.model;

import java.io.IOException;
import java.util.List;

import org.undercut.model.datafetcher.DataFetcher;
import org.undercut.model.datafetcher.DataFetcherImpl;
import org.undercut.model.instantcollection.DefaultInstantCollection;
import org.undercut.model.instantcollection.InstantCollection;
import org.undercut.model.logger.Logger;
import org.undercut.model.logger.LoggerImpl;
import org.undercut.model.observers.GenericExceptionObserver;
import org.undercut.model.observers.ModelExceptionsObserverManager;
import org.undercut.model.processingengine.ProcessingEngine;
import org.undercut.model.processingengine.ProcessingEngineImpl;
import org.undercut.model.utils.CheckIllegalArgument;
import org.undercut.view.utils.TabName;

/**
 * Class that implements the access methods to the application domain of usage. 
 */
public class UnderCutModelImpl extends ModelExceptionsObserverManager implements UnderCutModel {

    private static final Logger LOG = new LoggerImpl(); 
    private static final String ELABORATION_FAILURE_MESSAGE = "Elaboration failed";
    private static final String LOG_FAILURE_MESSAGE = "Log failed";

    // DELTA_LOGGING_TIME = 1 -> 50ms
    private static final Integer DELTA_LOGGING_TIME = 50;

    private final DataFetcher df;
    private final ProcessingEngine pe;


    private InstantCollection actualValues;
    private int logCount;
    private boolean haveToLog;

    /**
     * Class constructor of UnderCutModelImpl class.
     */
    public UnderCutModelImpl() {
        super();
        this.df = new DataFetcherImpl();
        this.pe = new ProcessingEngineImpl();
        this.logCount = 0;
        this.actualValues = DefaultInstantCollection.getDefaultInstantCollection();
    }

    @Override
    public InstantCollection getValues() {
        final InstantCollection lastValues = this.actualValues;
        this.startElaboration();
        return lastValues;
    }

    @Override
    public void reset() throws IOException {
        this.df.reset();
        this.actualValues = DefaultInstantCollection.getDefaultInstantCollection();
    }

    @Override
    public void addExcObserver(final GenericExceptionObserver obs) throws IllegalArgumentException {
        CheckIllegalArgument.checkForIllegalArgument(obs);
        super.addExceptionObserver(obs);
    }

    @Override
    public void setLogging(final List<TabName> tabsToLog) throws IOException {
        if (tabsToLog == null || tabsToLog.isEmpty()) {
            this.haveToLog = false;
        } else {
            this.haveToLog = true;
            UnderCutModelImpl.LOG.setWhatToLog(tabsToLog);
        }
    }

    /**
     * Method to start a new worker for a new data elaboration. 
     */
    private void startElaboration() throws RuntimeException {
        new ElaborationWorker().start();
    }

    /**
     * Method to check if it's time to do a log.
     * 
     * @return
     *          Boolean answer
     */
    private boolean checkIfLog() {
        this.logCount++;
        if (this.logCount >= UnderCutModelImpl.DELTA_LOGGING_TIME) {
            this.logCount = 0;
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * Class of objects whose job is to complete an entire data elaboration. It means that it deals with:
     * - Ask DataFetcher to rough collection of data;
     * - Commission ProcessingEngine to elaborate rough data into a refined InstantCollection.
     * When it finishes, the worker ends its execution and wait for GarbageCollector to delete it.
     */
    private class ElaborationWorker extends Thread {

        public void run() {
            try {
                actualValues = pe.processData(df.getValues());
                if (haveToLog && checkIfLog()) {
                    LOG.doLog(actualValues);
                }
            } catch (IllegalStateException | NumberFormatException | IOException e) {
                notifyExceptionObserver(e, UnderCutModelImpl.ELABORATION_FAILURE_MESSAGE);
            } catch (IllegalArgumentException e) {
                notifyExceptionObserver(e, UnderCutModelImpl.LOG_FAILURE_MESSAGE);
            }
        }

    }
}
