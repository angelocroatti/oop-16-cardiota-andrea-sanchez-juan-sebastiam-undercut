package org.undercut.model.instantcollection;

import java.util.Map;

import org.undercut.model.FieldName;
import org.undercut.view.utils.TabName;

/**
 * Interface to model the concept of a collection of values relative to a certain given time.
 */
public interface InstantCollection {

    /**
     * Only method to access the InstantCollection, it allows to get all the values of a certain organisation of data.
     * 
     * @param tabName
     *          The name of the tab which returns the values. 
     * 
     * @return
     *          An elaborated map that contains all the values (of the required tab) ready 
     *          to be shown to the user, organized by: FieldName, FieldValue.
     *
     * @throws IllegalArgumentException
     *          If the argument is null or equals to TabName.LOG
     */
    Map<FieldName, ? extends Number> getValuesOf(TabName tabName) throws IllegalArgumentException;

}
