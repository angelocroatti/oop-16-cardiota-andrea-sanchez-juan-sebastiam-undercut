package org.undercut.model.instantcollection;

import java.util.Arrays;
import java.util.HashMap;

import org.undercut.model.FieldName;
import org.undercut.model.utils.CheckIllegalArgument;
import org.undercut.view.utils.TabName;

/**
 * Class to allow the user to create and operate an InstantCollection.
 */
public final class InstantCollectionImpl extends AbstractInstantCollection {

    /**
     * Class constructor of the InstantCollectionImpl class.
     */
    public InstantCollectionImpl() {
        super();
        this.initCollection();
    }

    /**
     * Method to add a value to the actual InstantCollection, in its right location.
     * 
     * @param fieldName
     *          The value of the fieldName to set into the collection
     * 
     * @param value
     *          The value to be set
     *
     * @throws IllegalArgumentException
     *          If the fieldName to add is null
     */
    public void setValueOf(final FieldName fieldName, final Number value) throws IllegalArgumentException {
        CheckIllegalArgument.checkForIllegalArgument(fieldName);
        super.getInstantCollection().get(fieldName.getBelongsTab()).put(fieldName, value);
    }

    /**
     * Method to initialise the InstantCollection.
     */
    private void initCollection() {
        Arrays.asList(TabName.values()).stream().filter(t -> !t.equals(TabName.LOG)).forEach(tn -> super.getInstantCollection().put(tn, new HashMap<>()));
    }

}
