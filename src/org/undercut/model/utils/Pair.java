package org.undercut.model.utils;

/**
 * Class that models the concept of a pair of objects.
 *
 * @param <X>
 *      Type of the first parameter
 *
 * @param <Y>
 *      Type of the second parameter
 * 
 */
public class Pair<X, Y> {

    private final X first;
    private final Y second;

    /**
     * Constructor of the Pair<X, Y> class.
    *
    * @param x
    *           Pair's first value
    * @param y
    *           Pair's second value
    */
    public Pair(final X x, final Y y) {
        this.first = x;
        this.second = y;
    }

    /**
    * Access method to the pair's first value.
    * 
    * @return 
    *          Pair's first value
    */
    public X getFirst() {
        return this.first;
    }

    /**
     * Access method to the pair's second value.
     *
     * @return 
     *          Pair's second value
     */
    public Y getSecond() {
        return this.second;
    }

} 