package org.undercut.model.utils;

import org.undercut.view.utils.TabName;

/**
 * Class to check if arguments are correct.
 */
public final class CheckIllegalArgument {

    private CheckIllegalArgument() { }

    /**
     * Static method to check if passed-in arguments are invalid. 
     * 
     * @param obj
     *          The object to check integrity
     * 
     * @throws IllegalArgumentException
     *          If arguments are invalid
     */
    public static void checkForIllegalArgument(final Object obj) throws IllegalArgumentException {
        if (obj == null) {
            throw new IllegalArgumentException();
        } else {
            if (obj instanceof TabName && obj.equals(TabName.LOG)) {
                throw new IllegalArgumentException();
            }
        }
    }

}
