package org.undercut.view;

import org.undercut.view.tabs.vcu.VCULayout;
import org.undercut.view.utils.ExceptionAlertBox;
import org.undercut.view.utils.ExitAlertBox;
import org.undercut.view.utils.ScreenUtils;
import org.undercut.view.utils.ScreenUtilsImpl;
import org.undercut.view.utils.TabName;
import org.undercut.view.tabs.ecu.ECULayout;
import org.undercut.view.tabs.inertial.InertialLayout;
import org.undercut.view.tabs.log.LogLayout;
import org.undercut.controller.UnderCutController;
import org.undercut.model.instantcollection.InstantCollection;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.stage.Stage;

/**
 * View main class taking care of launching the main stage.
 */
public class UnderCutViewImpl implements UnderCutView {

    private static final double HEIGHT_SIZING_FACTOR = 0.99;
    private static final double WIDTH_SIZING_FACTOR = 0.99;
    private static final String STAGE_TITLE = "UnderCut";
    private final Map<TabName, LayoutSetUp> views;
    private Tab logTab;
    private Stage ucPrimaryStage;

    /**
     * Constructor.
     * 
     * @param controller
     *            the controller to follow.
     * 
     */
    public UnderCutViewImpl(final UnderCutController controller) {
        this.views = Collections.synchronizedMap(new HashMap<>());
        this.views.put(TabName.ECU, new ECULayout(controller));
        this.views.put(TabName.VCU, new VCULayout());
        this.views.put(TabName.INERTIAL, new InertialLayout());
        this.views.put(TabName.LOG, new LogLayout());
    }

    @Override
    public void start(final Stage primaryStage) {
        final ScreenUtils util = new ScreenUtilsImpl();
        this.ucPrimaryStage = primaryStage;
        this.ucPrimaryStage.setOnCloseRequest(e -> {
            new ExitAlertBox(e).display();
        });
        this.ucPrimaryStage.setTitle(STAGE_TITLE);
        this.ucPrimaryStage.setMinWidth(util.getScreenWidth() / WIDTH_SIZING_FACTOR);
        this.ucPrimaryStage.setMinHeight(util.getScreenHeight() / HEIGHT_SIZING_FACTOR);
        this.ucPrimaryStage.setMaxWidth(util.getScreenWidth() / WIDTH_SIZING_FACTOR);
        this.ucPrimaryStage.setMaxHeight(util.getScreenHeight() / HEIGHT_SIZING_FACTOR);
        this.initRootLayout();
    }

    @Override
    public void reset() {
        this.views.values().forEach(v -> v.reset());
    }

    @Override
    public void enableLogging() {
        this.logTab.setDisable(Boolean.FALSE);
    }

    @Override
    public void disableLogging() {
        this.logTab.setDisable(Boolean.TRUE);
    }

    @Override
    public List<TabName> getSelectedToLog() {
        return ((LogLayout) this.views.get(TabName.LOG)).getTabsSelected();
    }

    @Override
    public void update(final InstantCollection values) {
        this.views.entrySet().stream()
                             .filter(v -> !v.getKey().equals(TabName.LOG))
                             .forEach(e -> e.getValue().update(values.getValuesOf(e.getKey())));
    }

    @Override
    public void launchExceptionDialog(final String customWarningMessage,
                                      final EventHandler<ActionEvent> customButtonBehavior) {
        new ExceptionAlertBox(customWarningMessage, customButtonBehavior).display();
    }

    /**
     * Initializes the root layout.
     */
    private void initRootLayout() {
        final String cssStyling = "/style/root.css";
        final TabPane tabPane = new TabPane();
        final Scene scene = new Scene(tabPane);
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        tabPane.getTabs().addAll(this.createTabs());
        scene.getStylesheets().add(cssStyling);
        this.ucPrimaryStage.setScene(scene);
        this.ucPrimaryStage.setMaximized(true);
        this.ucPrimaryStage.show();
    }

    /**
     * Creates the tabs to display.
     * 
     * @return a collection of the created tabs.
     */
    private Collection<Tab> createTabs() {
        final Tab ecuTab = new Tab(TabName.ECU.toString());
        final Tab vcuTab = new Tab(TabName.VCU.toString());
        final Tab inertialTab = new Tab(TabName.INERTIAL.toString());
        this.logTab = new Tab(TabName.LOG.toString());
        ecuTab.setContent(this.views.get(TabName.ECU).addGaugesToPane());
        vcuTab.setContent(this.views.get(TabName.VCU).addGaugesToPane());
        inertialTab.setContent(this.views.get(TabName.INERTIAL).addGaugesToPane());
        this.logTab.setContent(this.views.get(TabName.LOG).addGaugesToPane());

        return Arrays.asList(ecuTab, vcuTab, inertialTab, logTab);
    }
}
