package org.undercut.view.utils;

/**
 * Interface for the custom alert box warning class.
 */
public interface AlertBox {

    /**
     * Displays the Alert Box.
     */
    void display();
}
