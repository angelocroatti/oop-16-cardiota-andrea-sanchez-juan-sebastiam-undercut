package org.undercut.view.utils;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Custom alert box class that's extended by actual custom utility alert box classes. 
 */
public abstract class AbstractAlertBox implements AlertBox {

    private static final String CSS_STYLING = "/style/alertbox.css";
    private static final String BUTTON_STYLE = "closeButton";
    private static final String TEXT_CANCEL_BUTTON = "Cancel";
    private static final int VERTICAL_SPACING = 20;
    private static final int HORIZONTAL_SPACING = 25;
    private static final int WIDTH_SIZING_FACTOR = 3;
    private static final int HEIGHT_SIZING_FACTOR = 6;

    private final Stage alertBox;
    private final HBox actionButtons;
    private final Event eventToManipulate;

    /**
     * Default constructor of the AbstractAlertBox class.
     * 
     * @param customTitle
     *            The alertBox's title
     * 
     * @param customWarningMessage
     *            The alertBox's message
     * 
     * @param customButtonMessage
     *            The alertBox's custom button text
     * 
     * @param event
     *            The event to manipulate
     * 
     * @param customButtonBehavior
     *            The alertBox's custom button behavior
     * 
     */
    public AbstractAlertBox(final String customTitle,
                            final String customWarningMessage,
                            final String customButtonMessage,
                            final Event event,
                            final EventHandler<ActionEvent> customButtonBehavior) {
        this.alertBox = new Stage();
        this.eventToManipulate = event;
        final ScreenUtils util = new ScreenUtilsImpl();
        final VBox container = new VBox(AbstractAlertBox.VERTICAL_SPACING);
        this.actionButtons = new HBox(AbstractAlertBox.HORIZONTAL_SPACING);
        final Scene scene = new Scene(container);
        final Button customButton = new Button(customButtonMessage);
        final Label label = new Label(customWarningMessage);
        final double alertWidth = util.getScreenWidth() / AbstractAlertBox.WIDTH_SIZING_FACTOR;
        final double alertHeight = util.getScreenHeight() / AbstractAlertBox.HEIGHT_SIZING_FACTOR;
        this.alertBox.setTitle(customTitle);
        this.alertBox.initModality(Modality.APPLICATION_MODAL);
        this.alertBox.setMinWidth(alertWidth);
        this.alertBox.setMinHeight(alertHeight);
        this.alertBox.resizableProperty().setValue(Boolean.FALSE);
        scene.getStylesheets().add(AbstractAlertBox.CSS_STYLING);
        this.alertBox.setOnCloseRequest(e -> {
            System.exit(0);
        });
        customButton.setDefaultButton(true);
        customButton.setOnAction(customButtonBehavior);
        this.actionButtons.setAlignment(Pos.CENTER);
        this.actionButtons.getChildren().add(customButton);
        container.getChildren().addAll(label, this.actionButtons);
        container.setAlignment(Pos.CENTER);
        this.alertBox.setScene(scene);
    }

    @Override
    public void display() {
        this.alertBox.showAndWait();
    }

    /**
     * Adds custom cancel button.
     */
    protected void addCancelButton() {
        final Button cancelButton = new Button(AbstractAlertBox.TEXT_CANCEL_BUTTON);
        cancelButton.getStyleClass().add(AbstractAlertBox.BUTTON_STYLE);
        cancelButton.setOnAction(e -> {
            this.setDefaultCancelOperation();
        });
        this.actionButtons.getChildren().add(0, cancelButton);
        this.alertBox.setOnCloseRequest(e -> {
            this.setDefaultCancelOperation();
        });
    }

    /**
     * Sets cancel button behaviour to default.
     */
    private void setDefaultCancelOperation() {
        this.eventToManipulate.consume();
        this.alertBox.close();
    }

}
