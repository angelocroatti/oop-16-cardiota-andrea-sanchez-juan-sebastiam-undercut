package org.undercut.view.utils;

import javafx.scene.Node;

/**
 * Interface for the ScreenUtils utility class.
 */
public interface ScreenUtils {

    /**
     * Gets the screen width.
     * 
     * @return the screen width
     */
    double getScreenWidth();

    /**
     * Gets the screen height.
     * 
     * @return the screen height
     */
    double getScreenHeight();

    /**
     * Places a component on the screen.
     * 
     * @param component
     *            component to place
     * @param xPos
     *            position on the x axis
     * @param yPos
     *            position on the y axis
     */
    void placeThisTo(Node component, double xPos, double yPos);

    /**
     * Computes the exact position to place a component based on screen
     * resolution percentage.
     * 
     * @param value
     *            the percentage of the screen width to use to place the
     *            component.
     * @return the exact start pixel to place the component to.
     */
    double computeX(double value);

    /**
     * Computes the exact position to place a component based on screen
     * resolution percentage.
     * 
     * @param value
     *            the percentage of the screen height to use to place the
     *            component.
     * @return the exact start pixel to place the component to.
     */
    double computeY(double value);

}
