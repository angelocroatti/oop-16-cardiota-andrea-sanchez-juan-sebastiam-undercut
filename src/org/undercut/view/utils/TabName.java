package org.undercut.view.utils;

/**
 * Enumaration used to better organize tab's names.
 */
public enum TabName {

    /**
     * ECU's name.
     **/
    ECU("ECU"),

    /**
     * VCU's tab name.
     **/
    VCU("VCU"),

    /**
     * Inertial's tab name.
     **/
    INERTIAL("INERTIAL"),

    /**
     * Log's tab name.
     **/
    LOG("LOG");

    private final String name;

    TabName(final String tabName) {
        this.name = tabName;
    }

    /**
     * 
     * @return the tab's name in a String format.
     */
    public String toString() {
        return this.name;
    }

}
