package org.undercut.view.utils;

import javafx.event.Event;

/**
 * Class defining alert boxes to confirm system exit.
 */
public class ExitAlertBox extends AbstractAlertBox {

    /**
     * Default constructor of ExitAlertBox class.
     *
     * @param event
     *            The event to manipulate
     */
    public ExitAlertBox(final Event event) {
        super("Confirm Exit", "Exit UnderCut?", "Exit", event, (e) -> System.exit(0));
        this.addCancelButton();
    }

}
