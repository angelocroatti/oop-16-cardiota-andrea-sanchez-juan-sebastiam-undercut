package org.undercut.view.utils;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;

/**
 * Class defining alert boxes to let the user know that an exception has occurred.
 */
public class ExceptionAlertBox extends AbstractAlertBox {

    /**
     * Default constructor of the ExceptionAlertBox class.
     * 
     * @param customWarningMessage
     *          The alertBox's message
     * 
     * @param customButtonBehavior
     *          The alertBox's custom button behavior
     */
    public ExceptionAlertBox(final String customWarningMessage,
                             final EventHandler<ActionEvent> customButtonBehavior) {
        super("EXCEPTION", customWarningMessage, "Restart", new Event(EventType.ROOT), customButtonBehavior);
    }

}
