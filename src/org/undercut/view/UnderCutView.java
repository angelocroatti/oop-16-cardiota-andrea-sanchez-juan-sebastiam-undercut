package org.undercut.view;

import java.util.List;

import org.undercut.model.instantcollection.InstantCollection;
import org.undercut.view.utils.TabName;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;

/**
 * Interface for the main View class.
 */
public interface UnderCutView {

    /**
     * Main stage launch method.
     * 
     * @param primaryStage
     *            Main Stage to launch, passed from the sw's Main class.
     */
    void start(Stage primaryStage);

    /**
     * Resets the view to its original state and form.
     */
    void reset();

    /**
     * Method to enable the log tab.
     */
    void enableLogging();

    /**
     * Method to disable the log tab.
     */
    void disableLogging();

    /**
     * Method to know what the user wants to log.
     * 
     * @return
     *          List of the tabs selected to be logged
     */
    List<TabName> getSelectedToLog();

    /**
     * @param values
     *            Values to show
     */
    void update(InstantCollection values);

    /**
     * @param customWarningMessage
     *            The message to set to the dialog to be shown
     * 
     * @param customButtonBehavior
     *            The behavior of the button to set to the dialog to be shown
     */
    void launchExceptionDialog(String customWarningMessage,
                               EventHandler<ActionEvent> customButtonBehavior);

}
