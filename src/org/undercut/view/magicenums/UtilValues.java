package org.undercut.view.magicenums;

/**
 * Enumeration class to avoid magic numbers in multiple properties building the
 * Gauges.
 * 
 * Every Gauge defining property contains respectively:
 * 1) Width defining factor to adapt UnderCut to every screen resolution is being used on;
 * 2) Height defining the same but for the screen height;
 * 3) Minimum Gauge property value (#);
 * 4) Maximum Gauge property value (#);
 * 5) X-position of a Gauge expressed in percentage;
 * 6) Y-position of a Gauge expressed in percentage.
 * 
 * (#) -> if available
 */
public enum UtilValues {

    /**
     * RPM Gauge property values.
     */
    RPM(3, 2, 0, 14000, 33, 0),

    /**
     * Fuel pressure Gauge property values.
     */
    FUEL_P(2, 0, 0, 5, 70, 8),

    /**
     * Airbox pressure Gauge property values.
     */
    AIR_BOX_P(2, 0, 0, 1, 70, 29),

    /**
     * Torque request Gauge property values.
     */
    TORQUE_REQ(7, 4, 0, 100, 12, 31),

    /**
     * Throttle Gauge property values.
     */
    THROTTLE(3.2, 2.2, 0, 0, 40, 47),

    /**
     * Brake Gauge property values.
     */
    BRAKE(3.2, 2.2, 0, 0, 50, 47),

    /**
     * Gear Gauge property values.
     */
    GEAR(10, 5, 0, 0, 14, 17),

    /**
     * Oil pressure Gauge property values.
     */
    OIL_P(3, 5.7, 0, 6, 2, 53),

    /**
     * Oil temperature Gauge property values.
     */
    OIL_T(3, 5.7, 10, 120, 2, 71),

    /**
     * Water temperature Gauge property values.
     */
    WATER_T(3, 5.7, 30, 110, 64.5, 71),

    /**
     * Air temperature Gauge property values.
     */
    AIR_T(3, 5.7, 20, 60, 64.5, 53),

    /**
     * Start button Gauge property values.
     */
    START(0, 0, 0, 0, 10, 10),

    /**
     * Stop button Gauge property values.
     */
    STOP(0, 0, 0, 0, 20, 10),

    /**
     * Gyro (on the X-axis) Gauge property values.
     */
    GYROX(5, 3, -100, 100, 17, 10),

    /**
     * Gyro (on the Y-axis) Gauge property values.
     */
    GYROY(5, 3, -100, 100, 39, 10),

    /**
     * Gyro (on the Z-axis) Gauge property values.
     */
    GYROZ(5, 3, -100, 100, 61, 10),

    /**
     * G-Force (on the X-axis) Gauge property values.
     */
    GX(2, 2, 0, 10, 14, 40),

    /**
     * G-Force (on the Y-axis) Gauge property values.
     */
    GY(2, 2, 0, 10, 40, 40),

    /**
     * G-Force (on the Z-axis) Gauge property values.
     */
    GZ(2, 2, 0, 10, 66, 40),

    /**
     * Last four Leaderboard Gauge property values.
     */
    LASTFOUR(2, 2.5, 0, 0, 1, 5),

    /**
     * Best four Leaderboard Gauge property values (set by the Lap action
     * button).
     */
    CLOCKED(2, 2.5, 0, 0, 1, 48),

    /**
     * Clock property values.
     */
    CLOCK(2, 2, 0, 0, 52, 35),

    /**
     * Chronobox Gauge property values.
     */
    CHRONOBOX(0, 0, 0, 0, 65, 5),

    /**
     * Front-Left brake temperature Gauge property values.
     */
    FLBRKTMP(5, 3, 0, 250, 34, 0),

    /**
     * Front-Right brake temperature Gauge property values.
     */
    FRBRKTMP(5, 3, 0, 250, 44, 0),

    /**
     * Rear-Left brake temperature Gauge property values.
     */
    RLBRKTMP(5, 3, 0, 250, 34, 35),

    /**
     * Rear-Right brake temperature Gauge property values.
     */
    RRBRKTMP(5, 3, 0, 250, 44, 35),

    /**
     * Front-Left suspension travel Gauge property values.
     */
    FLSUSP(5, 3, 0, 70, 54, 0),

    /**
     * Front-Right suspension travel Gauge property values.
     */
    FRSUSP(5, 3, 0, 70, 64, 0),

    /**
     * Rear-Left suspension travel Gauge property values.
     */
    RLSUSP(5, 3, 0, 70, 54, 35),

    /**
     * Rear-Right suspension travel Gauge property values.
     */
    RRSUSP(5, 3, 0, 70, 64, 35),

    /**
     * Front-Left wheel speed Gauge property values.
     */
    FLWHEEL(10, 5, 0, 0, 8, 3),

    /**
     * Front-Right wheel speed Gauge property values.
     */
    FRWHEEL(10, 5, 0, 0, 8, 16),

    /**
     * Rear-Left wheel speed Gauge property values.
     */
    RLWHEEL(10, 5, 0, 0, 81, 3),

    /**
     * Rear-Right wheel speed Gauge property values.
     */
    RRWHEEL(10, 5, 0, 0, 81, 16),

    /**
     * Slip differential (left) Gauge property values.
     */
    SLIPL(10, 5, 0, 0, 7, 70),

    /**
     * Slip differential (right) Gauge property values.
     */
    SLIPR(10, 5, 0, 0, 82, 70),

    /**
     * Brake balance Gauge property values.
     */
    BRKBIAS(3.5, 3.5, 0, 100, 2, 36),

    /**
     * Steering angle Gauge property values.
     */
    STRANGLE(3.5, 3.5, 0, 360, 77, 36),

    /**
     * Chassis temperature Gauge property values.
     */
    CHSSTMP(10, 5, 0, 0, 82, 80),

    /**
     * Front brakes pressure Gauge property values.
     */
    FRONTBRKP(5, 3, 0, 50, 24, 0),

    /**
     * Rear brakes pressure Gauge property values.
     */
    REARBRKP(5, 3, 0, 50, 24, 35),

    /**
     * Battery voltage Gauge property values.
     */
    BVOLT(5.8, 4.5, 11.8, 13.6, 23, 70),

    /**
     * Battery current Gauge property values.
     */
    BCURR(5.8, 4.5, 10, 25, 41, 70),

    /**
     * Current regulator Gauge property values.
     */
    REGCURR(5.8, 4.5, 150, 160, 59, 70),

    /**
     * Log CheckBox property values.
     */
    CHECKBOX(0, 0, 0, 0, 500, 20),

    /**
     * UniBo Motorsport logo property values.
     */
    LOGO(0, 0, 0, 0, 91.7, 3),

    /**
     * UniBo Motorsport big credits logo property values.
     */
    BIG_LOGO(0, 0, 0, 0, 38, 40),

    /**
     * Dialog of LOG tab property values.
     */
    LOGDIALOG(0, 0, 0, 0, 33, 2),

    /**
     * ECU option of LOG tab property values.
     */
    LOGECU(0, 0, 0, 0, 44, 8),

    /**
     * VCU option of LOG tab property values.
     */
    LOGVCU(0, 0, 0, 0, 44, 14),

    /**
     * Inertial option of LOG tab property values.
     */
    LOGINERTIAL(0, 0, 0, 0, 44, 20),

    /**
     * Cancel button of LOG tab property values.
     */
    LOGCANCEL(0, 0, 0, 0, 41.5, 27),

    /**
     * Set button of LOG tab property values.
     */
    LOGSET(0, 0, 0, 0, 51, 27),

    /**
     * UniBo Motorsport credits property values.
     */
    CREDITS_LABEL(0, 0, 0, 0, 28, 82),

    /**
     * UniBo Motorsport credits property values.
     */
    PATH_INFO(0, 0, 0, 0, 32, 34),

    /**
     * Andrea Cardiota contacts infos.
     */
    AC_CONTACTS(0, 0, 0, 0, 34.45, 87),

    /**
     * Juan Sebastiam Sanchez contacts infos.
     */
    JSS_CONTACTS(0, 0, 0, 0, 33, 89),

    /**
     * Engine temperature Gauge property values.
     */
    ENGINETMP(10, 5, 0, 0, 7, 80);

    private final double widthSizingFactor;
    private final double heightizingFactor;
    private final double minValue;
    private final double maxValue;
    private final double placingX;
    private final double placingY;

    UtilValues(final double width, final double height, final double min, final double max,
               final double x, final double y) {
        this.widthSizingFactor = width;
        this.heightizingFactor = height;
        this.minValue = min;
        this.maxValue = max;
        this.placingX = x;
        this.placingY = y;
    }

    /**
     * @return the width Gauge value.
     */
    public double getWidthFactor() {
        return this.widthSizingFactor;
    }

    /**
     * @return the height Gauge value.
     */
    public double getHeightFactor() {
        return this.heightizingFactor;
    }

    /**
     * @return the minimum Gauge value.
     */
    public double getMin() {
        return this.minValue;
    }

    /**
     * @return the maximum Gauge value.
     */
    public double getMax() {
        return this.maxValue;
    }

    /**
     * @return the x-axis position of a Gauge.
     */
    public double getX() {
        return this.placingX;
    }

    /**
     * @return the y-axis position of a Gauge.
     */
    public double getY() {
        return this.placingY;
    }
}
