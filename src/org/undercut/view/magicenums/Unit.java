package org.undercut.view.magicenums;

/**
 * Enumeration class to avoid magic numbers in the unit property building the
 * Gauges.
 */
public enum Unit {

    /**
     * RPM Gauge unit property value.
     */
    RPM("RPM"),

    /**
     * Fuel pressure Gauge unit property value.
     */
    PRESSURE("bar"),

    /**
     * Torque request Gauge unit property value.
     */
    TORQUE("Nm"),

    /**
     * Acceleration and Throotle Gauges unit property value.
     */
    PERC("%"),

    /**
     * Multiple temperature Gauges unit property value.
     */
    TMP("°C"),

    /**
     * Gauges Angle-unit property value.
     */
    ANGLE("°"),

    /**
     * Suspension Gauges unit property value.
     */
    SUSP("mm"),

    /**
     * Battery Gauges Volt unit property value.
     */
    VOLT("V"),

    /**
     * Battery Gauges Ampère unit property value.
     */
    AMP("A"),

    /**
     * Wheels speed Gauges unit property value.
     */
    SPEED("km/h"),

    /**
     * Gyros Gauges unit property value.
     */
    GYROS("rad"),

    /**
     * G_Force Gauges unit property value.
     */
    GFORCE("G");

    private String unit;

    Unit(final String gaugeUnit) {
        this.unit = gaugeUnit;
    }

    /**
     * @return the unit String of a Gauge.
     */
    public String getUnit() {
        return this.unit;
    }
}
