package org.undercut.view.magicenums;

/**
 * Enumeration class to avoid magic numbers in the angles property building the
 * Gauges.
 */
public enum Angles {

    /**
     * RPM Gauge angles property values.
     */
    RPM(320, 280),

    /**
     * Fuel pressure Gauge angles property values.
     */
    FUEL_P(110, 120),

    /**
     * Airbox pressure Gauge angles property values.
     */
    AIR_BOX_P(40, 150),

    /**
     * Inertial Gauges angles property values.
     */
    INERTIAL(248, 140),

    /**
     * Gyros Gauge angles property values.
     */
    GYROS(0, 4000);

    private final int startAngle;
    private final int angleRange;

    Angles(final int start, final int range) {
        this.startAngle = start;
        this.angleRange = range;
    }

    /**
     * @return the starting angle of a Gauge.
     */
    public double getStartingAngle() {
        return this.startAngle;
    }

    /**
     * @return the angle range of a Gauge.
     */
    public double getAngleRange() {
        return this.angleRange;
    }

}
