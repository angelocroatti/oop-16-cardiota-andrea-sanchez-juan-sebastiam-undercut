package org.undercut.view.tabs.ecu;

import org.undercut.controller.UnderCutController;
import org.undercut.model.FieldName;
import org.undercut.view.LayoutSetUpImpl;
import org.undercut.view.magicenums.Angles;
import org.undercut.view.magicenums.HEX;
import org.undercut.view.magicenums.Sections;
import org.undercut.view.magicenums.Unit;
import org.undercut.view.magicenums.UtilValues;
import org.undercut.view.utils.ScreenUtilsImpl;
import org.undercut.view.utils.TabName;

import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.GaugeBuilder;
import eu.hansolo.medusa.LcdDesign;
import eu.hansolo.medusa.LcdFont;
import eu.hansolo.medusa.Section;
import eu.hansolo.medusa.TickLabelLocation;
import eu.hansolo.medusa.TickMarkType;
import eu.hansolo.medusa.Gauge.KnobType;
import eu.hansolo.medusa.Gauge.NeedleBehavior;
import eu.hansolo.medusa.Gauge.NeedleShape;
import eu.hansolo.medusa.Gauge.NeedleSize;
import eu.hansolo.medusa.Gauge.NeedleType;
import eu.hansolo.medusa.Gauge.ScaleDirection;
import eu.hansolo.medusa.Gauge.SkinType;
import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.TileBuilder;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Stop;

/**
 * Building class to draw the ECU tab layout.
 */
public class ECULayout extends LayoutSetUpImpl {


    /**
     * Main constructor of the ECULayout class.
     * 
     * @param controller
     *          the controller used to set the components.     * 
     */
    public ECULayout(final UnderCutController controller) {
        super(controller, TabName.ECU);
    }

    @Override
    public Pane addGaugesToPane() {
        final int setZero = 0;
        final int setOne = 1;
        final int setTwo = 1;
        final int setTen = 10;
        final int airpMinorTickSpace = 15;
        final double rpmMediumTickMarkWidthFactor = 0.050;
        final double rpmMediumTickMarkLengthFactor = 0.05;
        final double rpmMajorTickMarkWidthFactor = 0.300;
        final double rpmMajorTickMarkLengthFactor = 0.35;
        final double minorTickMarkWidthFactor = 0.02;
        final Pane pane = new Pane();
        final Button start = new Button("Start simulation");
        final Button stop = new Button("Shut down");
        final ScreenUtilsImpl util = new ScreenUtilsImpl();
        final int optimalLogoFactor = 100;
        final double imgDimScaleFactor = util.getScreenWidth() / optimalLogoFactor;
        final double logoProportions = util.getScreenWidth() / imgDimScaleFactor;
        final ImageView uniBoMotorsportLogo = new ImageView(new Image(getClass().getResourceAsStream("/images/logo_UBM.png"), logoProportions, logoProportions, true, true));

        start.setOnAction(e -> {
            start.setDisable(true);
            stop.setDisable(false);
            super.getUcController().startUC();
        });

        stop.setDisable(true);
        stop.setOnAction(e -> {
            stop.setDisable(true);
            start.setDisable(false);
            super.getUcController().stopUC();
        });

        start.getStyleClass().add("ecuButtons");
        stop.getStyleClass().add("ecuButtons");

        final Gauge rpm = GaugeBuilder.create()
                                      .prefSize(util.getScreenWidth() / UtilValues.RPM.getWidthFactor(),
                                                util.getScreenHeight() / UtilValues.RPM.getHeightFactor())
                                      .title("Engine")
                                      .unit(Unit.RPM.getUnit())
                                      .scaleDirection(ScaleDirection.CLOCKWISE)
                                      .tickLabelLocation(TickLabelLocation.OUTSIDE)
                                      .startAngle(Angles.RPM.getStartingAngle())
                                      .angleRange(Angles.RPM.getAngleRange())
                                      .decimals(setZero)
                                      .minValue(UtilValues.RPM.getMin())
                                      .maxValue(UtilValues.RPM.getMax())
                                      .lcdVisible(true)
                                      .lcdDesign(LcdDesign.GRAY_PURPLE)
                                      .knobType(KnobType.METAL)
                                      .startFromZero(true)
                                      .tickLabelLocation(TickLabelLocation.INSIDE)
                                      .mediumTickMarksVisible(false)
                                      .mediumTickMarkWidthFactor(rpmMediumTickMarkWidthFactor)
                                      .mediumTickMarkLengthFactor(rpmMediumTickMarkLengthFactor)
                                      .majorTickMarkLengthFactor(rpmMajorTickMarkLengthFactor)
                                      .majorTickMarkWidthFactor(rpmMajorTickMarkWidthFactor)
                                      .majorTickMarkType(TickMarkType.LINE)
                                      .sectionsVisible(true)
                                      .sections(new Section(Sections.RPM_SECTION.getFirst(), Sections.RPM_SECTION.getSecond(), Color.YELLOW),
                                                new Section(Sections.RPM_SECTION.getSecond(), Sections.RPM_SECTION.getThird(), Color.ORANGE),
                                                new Section(Sections.RPM_SECTION.getThird(), Sections.RPM_SECTION.getFourth(), Color.RED),
                                                new Section(Sections.RPM_SECTION.getFourth(), Sections.RPM_SECTION.getFifth(), Color.DARKRED))
                                    .gradientBarEnabled(false)
                                    .gradientBarStops(new Stop(setZero, Color.GREEN),
                                                      new Stop(Sections.RPM_GRADIENT.getFirst(), Color.GREEN),
                                                      new Stop(Sections.RPM_GRADIENT.getSecond(), Color.GREEN),
                                                      new Stop(Sections.RPM_GRADIENT.getThird(), Color.GREEN),
                                                      new Stop(Sections.RPM_GRADIENT.getFourth(), Color.GREEN),
                                                      new Stop(Sections.RPM_GRADIENT.getFifth(), Color.GREEN))
                                    .areasVisible(true)
                                    .needleColor(Color.RED)
                                    .needleSize(NeedleSize.STANDARD)
                                    .needleShape(NeedleShape.ANGLED)
                                    .needleType(NeedleType.STANDARD)
                                    .needleBehavior(NeedleBehavior.OPTIMIZED)
                                    .build();
        super.getGauges().put(FieldName.RPM, rpm);

        final Gauge fuelPressure = GaugeBuilder.create()
                                                .maxSize(util.getScreenWidth() / UtilValues.FUEL_P.getWidthFactor(), 
                                                         util.getScreenHeight() / UtilValues.FUEL_P.getHeightFactor())
                                                .skinType(SkinType.HORIZONTAL)
                                                .angleRange(Angles.FUEL_P.getAngleRange())
                                                .startAngle(Angles.FUEL_P.getStartingAngle())
                                                .minValue(UtilValues.FUEL_P.getMin())
                                                .maxValue(UtilValues.FUEL_P.getMax())
                                                .decimals(setOne)
                                                .sectionsVisible(true)
                                                .sections(new Section(Sections.FUEL_P.getFirst(), Sections.FUEL_P.getSecond(), Color.ORANGE))
                                                .mediumTickMarksVisible(false)
                                                .minorTickMarkWidthFactor(minorTickMarkWidthFactor)
                                                .tickLabelDecimals(setOne)
                                                .tickLabelLocation(TickLabelLocation.OUTSIDE)
                                                .minorTickSpace(setTen)
                                                .needleType(NeedleType.AVIONIC)
                                                .title("Fuel pressure")
                                                .unit(Unit.PRESSURE.getUnit())
                                                .build();
        super.getGauges().put(FieldName.FUEL_P, fuelPressure);

        final Gauge airBoxPressure = GaugeBuilder.create()
                                                 .maxSize(util.getScreenWidth() / UtilValues.AIR_BOX_P.getWidthFactor(), 
                                                          util.getScreenHeight())
                                                 .skinType(SkinType.HORIZONTAL)
                                                 .angleRange(Angles.AIR_BOX_P.getAngleRange())
                                                 .startAngle(Angles.AIR_BOX_P.getStartingAngle())
                                                 .minValue(UtilValues.AIR_BOX_P.getMin())
                                                 .maxValue(UtilValues.AIR_BOX_P.getMax())
                                                 .tickLabelDecimals(setOne)
                                                 .decimals(setOne)
                                                 .sectionsVisible(true)
                                                 .sections(new Section(Sections.AIR_BOX_P.getFirst(), Sections.AIR_BOX_P.getSecond(), Color.ORANGE))
                                                 .mediumTickMarksVisible(false)
                                                 .minorTickMarkWidthFactor(minorTickMarkWidthFactor)
                                                 .tickLabelDecimals(setTwo)
                                                 .tickLabelLocation(TickLabelLocation.OUTSIDE)
                                                 .minorTickSpace(airpMinorTickSpace)
                                                 .needleType(NeedleType.AVIONIC)
                                                 .title("Airbox pressure")
                                                 .unit(Unit.PRESSURE.getUnit())
                                                 .build();
        super.getGauges().put(FieldName.AIRBOX_P, airBoxPressure);

        final Gauge torqueRequest = GaugeBuilder.create()
                                                .maxSize(util.getScreenWidth() / UtilValues.TORQUE_REQ.getWidthFactor(), 
                                                         util.getScreenHeight() / UtilValues.TORQUE_REQ.getHeightFactor())
                                                .skinType(SkinType.DASHBOARD)
                                                .minValue(UtilValues.TORQUE_REQ.getMin())
                                                .maxValue(UtilValues.TORQUE_REQ.getMax())
                                                .barColor(Color.CORNFLOWERBLUE)
                                                .title("Torque request")
                                                .unit(Unit.TORQUE.getUnit())
                                                .build();
        super.getGauges().put(FieldName.TORQUE_REQ, torqueRequest);

        final Gauge throttlePerc = GaugeBuilder.create()
                                               .maxSize(util.getScreenWidth() / UtilValues.THROTTLE.getWidthFactor(), 
                                                        util.getScreenHeight() / UtilValues.THROTTLE.getHeightFactor())
                                               .skinType(SkinType.LINEAR)
                                               .decimals(setZero)
                                               .lcdVisible(true)
                                               .lcdDesign(LcdDesign.GRAY_PURPLE)
                                               .barColor(Color.web(HEX.THROTTLE.getColor()))
                                               .majorTickSpace(setTen)
                                               .title("Throttle")
                                               .unit(Unit.PERC.getUnit())
                                               .build();
        super.getGauges().put(FieldName.THR, throttlePerc);

        final Gauge brakePerc = GaugeBuilder.create()
                                            .maxSize(util.getScreenWidth() / UtilValues.BRAKE.getWidthFactor(), 
                                                     util.getScreenHeight() / UtilValues.BRAKE.getHeightFactor())
                                            .skinType(SkinType.LINEAR)
                                            .decimals(setZero)
                                            .lcdVisible(true)
                                            .lcdDesign(LcdDesign.GRAY_PURPLE)
                                            .barColor(Color.web(HEX.BRAKE.getColor()))
                                            .majorTickSpace(setTen)
                                            .title("Brake")
                                            .unit(Unit.PERC.getUnit())
                                            .build();
        super.getGauges().put(FieldName.BRK, brakePerc);

        final Gauge gear = GaugeBuilder.create()
                                       .maxSize(util.getScreenWidth() / UtilValues.GEAR.getWidthFactor(), 
                                                util.getScreenHeight() / UtilValues.GEAR.getHeightFactor())
                                       .skinType(SkinType.LCD)
                                       .title("Gear")
                                       .lcdFont(LcdFont.ELEKTRA)
                                       .decimals(setZero)
                                       .lcdDesign(LcdDesign.BLACK_RED)
                                       .build();
        super.getGauges().put(FieldName.GEAR, gear);

        final Tile oilPressure = TileBuilder.create()
                                            .prefSize(util.getScreenWidth() / UtilValues.OIL_P.getWidthFactor(), 
                                                      util.getScreenHeight() / UtilValues.OIL_P.getHeightFactor())
                                            .skinType(Tile.SkinType.SPARK_LINE)
                                            .minValue(UtilValues.OIL_P.getMin())
                                            .maxValue(UtilValues.OIL_P.getMax())
                                            .title("Oil pressure")
                                            .unit(" " + Unit.PRESSURE.getUnit())
                                            .gradientStops(new Stop(Sections.TIME_GRAPHS.getFirst(), Tile.GREEN),
                                                           new Stop(Sections.TIME_GRAPHS.getSecond(), Tile.YELLOW),
                                                           new Stop(Sections.TIME_GRAPHS.getThird(), Tile.RED))
                                            .strokeWithGradient(true)
                                            .build();
        super.getGauges().put(FieldName.OIL_P, oilPressure);

        final Tile oilTemp = TileBuilder.create()
                                        .prefSize(util.getScreenWidth() / UtilValues.OIL_T.getWidthFactor(), 
                                                  util.getScreenHeight() / UtilValues.OIL_T.getHeightFactor())
                                        .skinType(Tile.SkinType.SPARK_LINE)
                                        .minValue(UtilValues.OIL_T.getMin())
                                        .maxValue(UtilValues.OIL_T.getMax())
                                        .title("Oil temperature")
                                        .unit(" " + Unit.TMP.getUnit())
                                        .gradientStops(new Stop(Sections.TIME_GRAPHS.getFirst(), Tile.GREEN),
                                                       new Stop(Sections.TIME_GRAPHS.getSecond(), Tile.YELLOW),
                                                       new Stop(Sections.TIME_GRAPHS.getThird(), Tile.RED))
                                        .strokeWithGradient(true)
                                        .build();
        super.getGauges().put(FieldName.OIL_T, oilTemp);

        final Tile waterTemp = TileBuilder.create()
                                          .prefSize(util.getScreenWidth() / UtilValues.WATER_T.getWidthFactor(), 
                                                    util.getScreenHeight() / UtilValues.WATER_T.getHeightFactor())
                                          .skinType(Tile.SkinType.SPARK_LINE)
                                          .minValue(UtilValues.WATER_T.getMin())
                                          .maxValue(UtilValues.WATER_T.getMax())
                                          .title("Water temperature")
                                          .unit(" " + Unit.TMP.getUnit())
                                          .gradientStops(new Stop(Sections.TIME_GRAPHS.getFirst(), Tile.GREEN),
                                                         new Stop(Sections.TIME_GRAPHS.getSecond(), Tile.YELLOW),
                                                         new Stop(Sections.TIME_GRAPHS.getThird(), Tile.RED))
                                          .strokeWithGradient(true)
                                          .build();
        super.getGauges().put(FieldName.H2O_T, waterTemp);

        final Tile airTemp = TileBuilder.create()
                                        .prefSize(util.getScreenWidth() / UtilValues.AIR_T.getWidthFactor(), 
                                                  util.getScreenHeight() / UtilValues.AIR_T.getHeightFactor())
                                        .skinType(Tile.SkinType.SPARK_LINE)
                                        .minValue(UtilValues.AIR_T.getMin())
                                        .maxValue(UtilValues.AIR_T.getMax())
                                        .title("Air temperature")
                                        .unit(" " + Unit.TMP.getUnit())
                                        .gradientStops(new Stop(Sections.TIME_GRAPHS.getFirst(), Tile.GREEN),
                                                       new Stop(Sections.TIME_GRAPHS.getSecond(), Tile.YELLOW),
                                                       new Stop(Sections.TIME_GRAPHS.getThird(), Tile.RED))
                                        .strokeWithGradient(true)
                                        .build();
        super.getGauges().put(FieldName.AIR_T, airTemp);

        util.placeThisTo(rpm, UtilValues.RPM.getX(), UtilValues.RPM.getY());
        util.placeThisTo(throttlePerc, UtilValues.THROTTLE.getX(), UtilValues.THROTTLE.getY());
        util.placeThisTo(brakePerc, UtilValues.BRAKE.getX(), UtilValues.BRAKE.getY());
        util.placeThisTo(oilPressure, UtilValues.OIL_P.getX(), UtilValues.OIL_P.getY());
        util.placeThisTo(oilTemp, UtilValues.OIL_T.getX(), UtilValues.OIL_T.getY());
        util.placeThisTo(airTemp, UtilValues.AIR_T.getX(), UtilValues.AIR_T.getY());
        util.placeThisTo(waterTemp, UtilValues.WATER_T.getX(), UtilValues.WATER_T.getY());
        util.placeThisTo(gear, UtilValues.GEAR.getX(), UtilValues.GEAR.getY());
        util.placeThisTo(fuelPressure, UtilValues.FUEL_P.getX(), UtilValues.FUEL_P.getY());
        util.placeThisTo(airBoxPressure, UtilValues.AIR_BOX_P.getX(), UtilValues.AIR_BOX_P.getY());
        util.placeThisTo(torqueRequest, UtilValues.TORQUE_REQ.getX(), UtilValues.TORQUE_REQ.getY());
        util.placeThisTo(start, UtilValues.START.getX(), UtilValues.START.getY());
        util.placeThisTo(stop, UtilValues.STOP.getX(), UtilValues.STOP.getY());
        util.placeThisTo(uniBoMotorsportLogo, UtilValues.LOGO.getX(), UtilValues.LOGO.getY());

        pane.getChildren().addAll(start,
                                  stop, 
                                  rpm, 
                                  throttlePerc, 
                                  brakePerc, 
                                  oilPressure, 
                                  oilTemp, 
                                  airTemp, 
                                  waterTemp, 
                                  gear, 
                                  airBoxPressure, 
                                  fuelPressure, 
                                  torqueRequest,
                                  uniBoMotorsportLogo);
        return pane;
    }

}
