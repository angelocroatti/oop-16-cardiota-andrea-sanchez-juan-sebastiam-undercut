package org.undercut.controller;

import org.undercut.view.UnderCutView;

/**
 * Models the application's main controller.
 */
public interface UnderCutController {

    /**
     * Main method to start the graphical refresh.
     */
    void startUC();

    /**
     * Main method to stop the graphical refresh and reset the application.
     */
    void stopUC();

    /**
     * Method to register a view to manage.
     * 
     * @param view
     *            The view to register
     */
    void registerView(UnderCutView view);

}
