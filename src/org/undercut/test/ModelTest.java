package org.undercut.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;
import org.undercut.model.UnderCutModel;
import org.undercut.model.UnderCutModelImpl;

/**
 * Test class of the entire model.
 */
public class ModelTest {

    private static final Integer TOT_REQUEST = 200;

    /**
     * Basic test of model behavior.
     */
    @Test
    public void basicModelTest() {
        final UnderCutModel ucModel = new UnderCutModelImpl();
        if (ucModel.getValues() == null) {
            fail("The result of getValue() shouldn't be null.");
        }
        try {
            // It takes time to fetch the data in the model sub-thread.
            Thread.sleep(3L);
            ucModel.reset();
        } catch (IOException | InterruptedException e) {
            fail("There's a normal situation test, should not be any problems.");
        }
    }

    /**
     * Basic test of multiple values requests.
     */
    @Test
    public void multipleValueRequestsModelTest() {
        final UnderCutModel ucModel = new UnderCutModelImpl();
        for (int i = 0; i < ModelTest.TOT_REQUEST; i++) {
            if (ucModel.getValues() == null) {
                fail("The result of getValue() shouldn't be null.");
            }
        }
    }

    /**
     * Basic test of multiple reset requests.
     */
    @Test
    public void multipleResetRequestsModelTest() {
        final UnderCutModel ucModel = new UnderCutModelImpl();
        try {
            for (int i = 0; i < ModelTest.TOT_REQUEST; i++) {
                ucModel.reset();
            }
        } catch (IOException e) {
            fail("There's a normal situation test, should not be any problems.");
        }
    }

    /**
     * Test of setting what to log operation.
     */
    @Test
    public void setWhatToLogModelTest() {
        final UnderCutModel ucModel = new UnderCutModelImpl();
        try {
            ucModel.setLogging(null);
            ucModel.setLogging(new ArrayList<>());
        } catch (Exception e) {
            fail("There's a normal situation test, should not be any problems "
                 + "(null or empty list as argument means that there's nothig to log).");
        }
    }

    /**
     * Test of adding observer operation.
     */
    @Test
    public void addObserverModelTest() {
        final UnderCutModel ucModel = new UnderCutModelImpl();
        try {
            ucModel.addExcObserver(null);
            fail("Should throw an IllegalArgumentException.");
        } catch (IllegalArgumentException e) {
            assertNotNull(e);
        }
    }

}
