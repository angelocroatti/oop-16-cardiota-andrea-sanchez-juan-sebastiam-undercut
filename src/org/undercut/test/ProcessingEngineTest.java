package org.undercut.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.HashMap;

import org.junit.Test;
import org.undercut.model.datafetcher.DataFetcherImpl;
import org.undercut.model.instantcollection.InstantCollection;
import org.undercut.model.processingengine.ProcessingEngine;
import org.undercut.model.processingengine.ProcessingEngineImpl;
import org.undercut.view.utils.TabName;

/**
 * Test class to validate the correct data elaboration.
 */
public class ProcessingEngineTest {

    /**
     * Test of basic data elaboration.
     */
    @Test
    public void basicProcessingEngineTest() {
        final ProcessingEngine pe = new ProcessingEngineImpl();
        try {
            final InstantCollection ic = pe.processData(new DataFetcherImpl().getValues());
            for (final TabName tab : TabName.values()) {
                if (!tab.equals(TabName.LOG) && (ic.getValuesOf(tab).isEmpty())) {
                    fail("A data elaboration started from the result of a DataFetcher getValue()"
                         + " should not return an empty collection");
                }
            }
        } catch (IllegalArgumentException | IllegalStateException | IOException e) {
            fail("There shouldn't be any problems.");
        }
    }

    /**
     * Test of errors on basic data elaboration.
     */
    @Test
    public void errorsOnProcessingEngineTest() {
        final ProcessingEngine pe = new ProcessingEngineImpl();
        try {
            pe.processData(null);
            fail("Should throw a IllegalArgumentException.");
        } catch (IllegalArgumentException e) {
            assertNotNull(e);
        }
        try {
            pe.processData(new HashMap<>());
            fail("Should throw a IllegalArgumentException.");
        } catch (IllegalArgumentException e) {
            assertNotNull(e);
        }
    }

}
