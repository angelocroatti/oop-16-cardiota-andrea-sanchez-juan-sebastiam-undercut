package org.undercut.test;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Map;

import org.junit.Test;
import org.undercut.model.FieldName;
import org.undercut.model.datafetcher.DataFetcher;
import org.undercut.model.datafetcher.DataFetcherImpl;

/**
 * Class due to test the correct behavior of the DataFetcher system.
 */
public class DataFetcherTest {

    private static final Integer TOT_READS = 1000;
    private static final Integer TOT_RESETS = 100;

    /**
     * Testing single reading operation in a normal situation.
     */
    @Test
    public void singleReadTest() {
        final DataFetcher df = new DataFetcherImpl();
        try {
            final Map<FieldName, ? extends Number> values = df.getValues();
            if (values == null || values.isEmpty()) {
                fail("The collection couldn't be null or empty.");
            }
        } catch (NumberFormatException | IllegalStateException | IOException e) {
            fail("There's a normal situation test, should not be any problems.");
        }
    }

    /**
     * Testing multiple reading operation in a normal situation.
     */
    @Test
    public void multipleReadTest() {
        final DataFetcher df = new DataFetcherImpl();
        try {
            for (int i = 0; i < DataFetcherTest.TOT_READS; i++) {
                final Map<FieldName, ? extends Number> values = df.getValues();
                if (values == null || values.isEmpty()) {
                    fail("The collection couldn't be null or empty.");
                }
            }
        } catch (NumberFormatException | IllegalStateException | IOException e) {
            fail("There's a normal situation test, should not be any problems.");
        }
    }

    /**
     * Testing of a single reset operation.
     */
    @Test
    public void singleResetTest() {
        final DataFetcher df = new DataFetcherImpl();
        try {
            df.getValues();
            df.reset();
        } catch (IOException e) {
            fail("Normal reset operation should not cause any trouble.");
        }
    }

    /**
     * Testing of multiple reset operation.
     */
    @Test
    public void multipleResetTest() {
        final DataFetcher df = new DataFetcherImpl();
        try {
            df.getValues();
            for (int i = 0; i < DataFetcherTest.TOT_RESETS; i++) {
                df.reset();
            }
        } catch (IOException e) {
            fail("Multiple reset operations should not cause any trouble.");
        }
    }

}
